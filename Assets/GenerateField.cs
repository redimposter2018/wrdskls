using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GenerateField : MonoBehaviour
{
    private const int x = 10;
    private const int y = 10;
    [SerializeField] private Tilemap field;
    [SerializeField] private Tile cell;
    [SerializeField] private List<Vector3Int> neighbours;

    private void Start()
    {
        Generate();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int cellPos = field.WorldToCell(clickPos);
            print(cellPos);
            FindNeighbours(cellPos);
        }
    }

    void FindNeighbours(Vector3Int cellPos)
    {
        //Нахождение соседей
        neighbours.Add(new Vector3Int(cellPos.x - 1, cellPos.y, 0));
        neighbours.Add(new Vector3Int(cellPos.x + 1, cellPos.y, 0));
        neighbours.Add(new Vector3Int(cellPos.x, cellPos.y - 1, 0));
        neighbours.Add(new Vector3Int(cellPos.x, cellPos.y + 1, 0));
        //Проверка на четность Y
        if (cellPos.y % 2 == 0 || cellPos.y == 0)
        {
            neighbours.Add(new Vector3Int(cellPos.x - 1, cellPos.y - 1, 0));
            neighbours.Add(new Vector3Int(cellPos.x - 1, cellPos.y + 1, 0));
        }
        else
        {
            neighbours.Add(new Vector3Int(cellPos.x + 1, cellPos.y - 1, 0));
            neighbours.Add(new Vector3Int(cellPos.x + 1, cellPos.y + 1, 0));
        }
        //Удаление соседей выходящих за пределы поля
        List<Vector3Int> forDelete = new List<Vector3Int>();
        foreach (Vector3Int cell in neighbours)
        {
            if (cell.x >= x || cell.y >= y || cell.x < 0 || cell.y < 0) forDelete.Add(cell);
        }
        foreach (Vector3Int a in forDelete)
        {
            neighbours.Remove(a);
        }
        forDelete.Clear();
    }
    //Генерация поля
    private void Generate()
    {
        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                field.SetTile(new Vector3Int(j, i, 0), cell);
            }

        }
    }
}
